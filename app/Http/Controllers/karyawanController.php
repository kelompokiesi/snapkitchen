<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\karyawanModel;
use App\Models\akunModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class karyawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Halaman Login',
        ];
        return view('pages/login', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function log(Request $request)
    {
        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])) {
            $id = Auth::user()->id;
            $akun = akunModel::all()->find($id);
            $request->session()->regenerate();
            $request->session()->put('id_akun', $id);
            $request->session()->put('role', $akun['role']);
            $request->session()->put('username', $akun['username']);
            // dd(session()->get('username'));
            return redirect()->intended('/barang');
        } else {
            // dd('login gagal');
            return back()->with('loginError', 'Login Gagal!');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            akunModel::create([
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'role' => 'admin',
            ]);
            $akun = akunModel::orderBy('id', 'DESC')->first();
            // dd($akun);
            // dd($akun);php artisan migrate:fresh
            karyawanModel::create([
                'nama_karyawan' => $request->nama,
                'kontak_karyawan' => $request->kontak,
                'akun_id' => $akun['id'],
            ]);
            return redirect('/login');
        } catch (\Illuminate\Database\QueryException $ex) {
            return back()->with('unik', 'Username Sudah Dipakai, silahkan pilih username lain');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cust = karyawanModel::all();
        // $cust = $cust->find($id);
        $cust = DB::table('karyawan')
            ->join('akun', 'karyawan.akun_id', '=', 'akun.id')
            ->where('karyawan.akun_id', '=', $id)
            ->select('akun.username', 'karyawan.nama_karyawan', 'karyawan.kontak_karyawan', 'karyawan.akun_id', 'karyawan.id')->first();
        // dd($cust);
        $data = [
            'title' => 'Profile Admin',
            'content' => $cust,
        ];

        // dd($supp);
        return view('/pages/profile', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->input('username'));

        $karya = karyawanModel::all()->find($id);
        $karya->nama_karyawan = $request->input('nama');
        $karya->kontak_karyawan = $request->input('kontak');
        $karya->update();

        $admin = akunModel::all()->find($request->input('id'));
        // dd($admin);
        $admin->username = $request->input('username');
        $admin->update();


        return redirect('admin/' . $id)->with('success', 'Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
